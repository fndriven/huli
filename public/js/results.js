import { $klist, $switch } from "https://cdn.skypack.dev/pin/@thi.ng/rdom@v0.12.17-Nt3mAjilWEeIgVIBizpp/mode=imports,min/optimized/@thi.ng/rdom.js";
import { searchLangId, searchResults, linkView } from "./state.js";

const createUrl = (pid) => {
    return "https://" + searchLangId.deref() + linkView.deref() + ".wikipedia.org/?curid=" + String(pid);
};

$switch(
    searchResults,
    (x) => {
        if (!x.length) {
            return "empty";
        } else {
            return "notempty";
        }
    },
    {
        "empty": async (x) => {
            return ["ul", {}, ["li", {style: "text-align: center;"}, "Nothing found ..."]];
        },
        "notempty": async (x) => $klist(
            searchResults,
            "ul",
            {},
            (x) =>
                ["li", {},
                    ["h1", {},
                        ["a", {
                            href: createUrl(x.pageid),
                            target: "_blank"
                        }, x.title]],
                    ["div", {},
                        x.snippet.replace(/(<([^>]+)>)/gi, '')],
                ],
            (x) => `${x.title}-${x.pageid}`
        )
    },
    async (err) => ["div", {}, err],
   // async () => ["div", {}, "Loading..."]
).mount(document.getElementById("main"))