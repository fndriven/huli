import { searchString, searchResults, searchLangId, maxEntries } from "./state.js";

const callApi = (url) => {
    fetch(url)
        .then(response => response.json())
        .then(response => searchResults.next(response.query.search))
        .catch(error => console.log(error));
};

const baseUrl = () => { return "https://" + searchLangId.deref() + ".wikipedia.org/w/api.php" + "?origin=*" };

const defaultParams = {
    action: "query",
    list: "search",
    format: "json"
};

const params = () => {
    return Object.assign(
        defaultParams,
        {
            srsearch: searchString.deref(),
            srlimit: maxEntries.deref()
        });
};

const queryUrl = () => {
    let url = baseUrl();
    Object.keys(params()).forEach( key => {
        url += "&" + key + "=" + params()[key];
    });
    return url;
};

export { callApi, queryUrl };