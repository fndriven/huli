import { reactive } from "https://cdn.skypack.dev/pin/@thi.ng/rstream@v8.2.4-Nepkzv13Rhsx1BcZmf4Y/mode=imports,min/optimized/@thi.ng/rstream.js";
import { searchLanguages } from "./utils/languages.js";
import { setTheme, setLang } from "./utils/ui.js";

//                                             Helpers                                             //
const saveToLocalStorage = (arr) => {
    localStorage.setItem(arr[0], arr[1]);
};

//                                              State                                              //
// Search
const searchString = reactive("").map((s) => s.trim());
const searchResults = reactive([]);

// Settings 
const showSettingsDialog = reactive(false);

// searchLang
const searchLangId = reactive(localStorage.getItem('searchLangId') || "en");
searchLangId.map((x) => ["searchLangId", x]).subscribe({ next: saveToLocalStorage });
searchLangId.subscribe({next: setLang});
const searchLangName = searchLangId.map((x) => searchLanguages.find((l) => l.id == x).name)

// maxEntries
const maxEntries = reactive(localStorage.getItem('maxEntries') || 50);
maxEntries.map((x) => ["maxEntries", x]).subscribe({ next: saveToLocalStorage });

// linkView Open wiki links in mobile ".m" or not ""
const linkView = reactive(localStorage.getItem('linkView') || "");
linkView.map((x) => ["linkView", x]).subscribe({ next: saveToLocalStorage });

// Theme
const useDarkTheme = reactive(localStorage.getItem('useDarkTheme') || false).map((x) => JSON.parse(x));
useDarkTheme.map((x) => ["useDarkTheme", x]).subscribe({ next: saveToLocalStorage });
useDarkTheme.subscribe({ next: setTheme })

export {
    searchLangId,
    searchLangName,
    maxEntries,
    searchString,
    searchResults,
    useDarkTheme,
    showSettingsDialog,
    linkView
};