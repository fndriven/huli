const searchLanguages = [
    { id: "ar", name: "Arabic", text: "Arabic - العربية", direction: "rtl" },
    { id: "nl", name: "Dutch", text: "Dutch - Nederlands", direction: "ltr" },
    { id: "en", name: "English", text: "English", direction: "ltr" },
    { id: "fr", name: "French", text: "French - Français", direction: "ltr" },
    { id: "de", name: "German", text: "German - Deutsch", direction: "ltr" },
    { id: "he", name: "Hebrew", text: "Hebrew - עברית", direction: "rtl" }
];

export { searchLanguages };