import { $attribs } from "https://cdn.skypack.dev/pin/@thi.ng/rdom@v0.12.17-Nt3mAjilWEeIgVIBizpp/mode=imports,min/optimized/@thi.ng/rdom.js";
import { searchLanguages } from "./languages.js";

const screenSize = () => {
    const mediaIsSmall = window.matchMedia('(max-width: 600px)');
    const mediaIsMedium = window.matchMedia('(max-width: 992px)');
    if (mediaIsSmall.matches) {
        return "small";
      }
    if (mediaIsMedium.matches) {
        return "medium";
     } 
return "large";
}

const setLang = (id) => {
    const direction = searchLanguages.find((l) => l.id == id).direction;
    const html = document.querySelector("html");
    $attribs(html, { "dir": direction, "lang": id});
}

const setTheme = (bool) => {
    const html = document.querySelector("html");
    if (bool === true) {
        $attribs(html, { class: "sl-theme-dark" });
    } else {
        $attribs(html, { "class": "" });
    }
};

export { setTheme, setLang, screenSize };