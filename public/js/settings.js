import { $compile, $klist } from "https://cdn.skypack.dev/pin/@thi.ng/rdom@v0.12.17-Nt3mAjilWEeIgVIBizpp/mode=imports,min/optimized/@thi.ng/rdom.js";
import { reactive } from "https://cdn.skypack.dev/pin/@thi.ng/rstream@v8.2.4-Nepkzv13Rhsx1BcZmf4Y/mode=imports,min/optimized/@thi.ng/rstream.js";
import { searchLanguages } from "./utils/languages.js";
import { searchLangId, maxEntries, useDarkTheme, searchLangName, showSettingsDialog, linkView } from "./state.js";
import { screenSize } from "./utils/ui.js";

$compile(
    ["sl-dialog", {
        id: "settingsDialog",
        open: showSettingsDialog,
        label: "Settings",
        class: "dialog-width dialog-scrolling"
    },
        ["div", { style: "height: 50vh; border: dashed 2px var(--sl-color-neutral-200); padding: 1rem 1rem;" },
            ["sl-select", {
                id: "searchlang",
                label: "Search language",
                "help-text": searchLangName,
                placeholder: "Select a language",
                size: screenSize(),
                value: searchLangId.deref(),
                "onsl-change": (e) => {
                    searchLangId.next(e.target.value);
                }
            },
                $klist(
                    reactive(searchLanguages),
                    "div",
                    {},
                    (x) => ["sl-option", { value: x.id }, x.text],
                    (x) => `${x.id}-${x.text}`
                ),
            ],
            ["br"],
            ["sl-range", {
                min: 1,
                max: 120,
                value: maxEntries.deref(),
                label: "Max entries shown",
                size: screenSize(),
                "help-text": maxEntries,
                "onsl-change": (e) => {
                    maxEntries.next(e.target.value);
                }
            }],
            ["br"],
            ["sl-checkbox", {
                value: linkView,
                checked: linkView.deref(),
                size: screenSize(),
                "onsl-input": (e) => {
                    if (e.target.value === ".m") {
                        linkView.next("");
                    } else {
                        linkView.next(".m");
                    }
                }
            }, "Open Wikipedia links in mobile view"],
            ["br"],
            ["sl-checkbox", {
                value: useDarkTheme,
                checked: useDarkTheme.deref(),
                size: screenSize(),
                "onsl-input": (e) => {
                    if (e.target.value === false) {
                        useDarkTheme.next(true);
                    } else {
                        useDarkTheme.next(false);
                    }
                }
            }, "Use Dark Theme"],
        ],
        ["sl-button", {
            slot: "footer",
            variant: "primary",
            size: screenSize(),
            onclick: (e) => showSettingsDialog.next(false)
        }, "Close"]]
).mount(document.body);