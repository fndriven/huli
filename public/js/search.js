import { $compile } from "https://cdn.skypack.dev/pin/@thi.ng/rdom@v0.12.17-Nt3mAjilWEeIgVIBizpp/mode=imports,min/optimized/@thi.ng/rdom.js";
import { callApi, queryUrl } from "./wikiSearch.js";
import { searchString, searchLangName, showSettingsDialog } from "./state.js";
import { screenSize } from "./utils/ui.js";

$compile(
    ["div", { class: "animation-form" },
        ["sl-animation", { name: "headShake", duration: 1000, iterations: 1 },
            ["sl-input", {
                clearable: true,
                placeholder: searchLangName.map((x) => "Search Wikipedia (" + x + ")"),
                size: screenSize(),
                pill: true,
                // Events //
                "onsl-input": (e) => {
                    searchString.next(e.target.value);
                },
                "onsl-clear": (e) => { animation.play = true; },
                "onkeydown": (e) => {
                    const keyName = e.key;
                    if (keyName === "Enter") {
                        if (searchString.deref() != "") {
                            callApi(queryUrl());
                            animation.play = true;
                        }
                    }
                }
            },
                ["sl-icon", {
                    name: "search",
                    slot: "prefix"
                }],
                ["sl-icon-button", {
                    id: "gear",
                    name: "gear",
                    label: "Settings",
                    class: "gear",
                    slot: "suffix",
                    onclick: (e) => showSettingsDialog.next(true)
                }]
            ]
        ]
    ])
    .mount(document.getElementById('header'));

const container = document.querySelector('.animation-form');
const animation = container.querySelector('sl-animation');