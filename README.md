# Huli - Search Wikipedia
Huli* is a Web frontend for Wikipedia searches.

https://huli.fndriven.de/

## Dev requirements
Python3 or babashka or whatever to serve static assets.

A minimal Single Page App and client-only. Made with Vanilla Javascript, rdom and rstream from thin.ng/umbrella,
some shoelace Web Components and skypack imports. No bundler/JS tooling needed!

```bash
python3 -m http.server --directory "public" # or
bb dev
```

## Docs
- https://css-tricks.com/snippets/css/complete-guide-grid/
- https://shoelace.style/
- https://css-tricks.com/shoelace-component-frameowrk-introduction/
- https://docs.thi.ng/umbrella/rdom/index.html
- https://docs.thi.ng/umbrella/rstream/index.html
- https://docs.skypack.dev/skypack-cdn/api-reference/pinned-urls-optimized

## License
TBD

---
[*] https://hilo.hawaii.edu/wehe/?q=huli